<?php
declare(strict_types=1);
namespace App\Handler;

use App\Message\UserCreate;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class UserCreateHandler
{
    public function __construct(
        protected LoggerInterface $logger,
    ) {}

    public function __invoke(UserCreate $userCreate): void
    {
        $this->logger->warning('MICROSERVICE2: {USER_CREATED} - '.$userCreate->getEmail());
    }
}