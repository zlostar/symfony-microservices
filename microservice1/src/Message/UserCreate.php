<?php
declare(strict_types=1);
namespace App\Message;

class UserCreate
{
    public function __construct(
        protected ?string $email,
        protected ?string $firstName,
        protected ?string $lastName
    ){}

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function __serialize(): array
    {
        return [
            'email' => $this->email,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName
        ];
    }
}