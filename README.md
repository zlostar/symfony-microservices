
NextBasket task
========================



## Local Development
- Start the application locally: docker-compose up --build -d
- docker ps - get list of containers
- Install dependencies and recipes of each microservice: docker exec -it <containerId> composer install
- call POST http://localhost:9000/api/users with form data (email, firstName, lastName) postman collection provided in the email
- on microservice2 you can also use php bin/console app:create-user instead of the api to create a user and generate a message to the rabbitMQ
- on microservice1 call php bin/console messenger:consume -vv external_messages to process the generated rabbitMQ data


### Run tests
- docker ps - get list of containers
- docker exec -it <containerId> php bin/phpunit


