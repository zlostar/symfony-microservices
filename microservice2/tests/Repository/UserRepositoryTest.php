<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use App\Message\UserCreate;
use App\Repository\UserRepository;
use App\Repository\UserRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @internal
 */
class UserRepositoryTest extends TestCase
{
    private UserRepositoryInterface $userRepository;
    private MessageBusInterface $messageBus;

    public function setUp(): void
    {
        $this->userRepository = $this->createMock(UserRepositoryInterface::class);
        $this->messageBus = $this->createMock(MessageBusInterface::class);
    }

    public function testUserRepository(): void
    {
        $message = new UserCreate(
            'viktor.georgiev@outlook.com',
            'Viktor',
            'Georgiev'
        );

        $repository = new UserRepository();

        $countBefore = count(file($repository->getPath()));

        $repository->create($message);

        $countAfter = count(file($repository->getPath()));

        $this->assertEquals($countBefore+1, $countAfter);
    }
}