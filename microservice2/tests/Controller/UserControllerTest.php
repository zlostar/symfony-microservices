<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\UserController;
use App\Entity\User;
use App\Service\UserServiceInterface;
use JMS\Serializer\Exception\ValidationFailedException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Envelope;
use App\Message\UserCreate;

/**
 * @internal
 */
class UserControllerTest extends TestCase
{
    private UserServiceInterface $userService;

    public function setUp(): void
    {
        $this->userService = $this->createMock(UserServiceInterface::class);
    }

    public function testUserCreation(): void
    {
        $message = new UserCreate(
            'viktor.georgiev@outlook.com',
            'Viktor',
            'Georgiev'
        );

        $this->userService
            ->expects(self::once())
            ->method('create')
            ->willReturn(new Envelope($message, []));

        $controller = new UserController($this->userService);

        $request = new Request();
        $request->headers->set('Content-type', 'application/json');
        $request->setMethod('POST');
        $request->request->set('email', 'viktor.georgiev@outlook.com');
        $request->request->set('firstName', 'Viktor');
        $request->request->set('lastName', 'Georgiev');

        $response = $controller->create($request);

        $this->assertEquals($response->getContent(), 'O:22:"App\Message\UserCreate":3:{s:5:"email";s:27:"viktor.georgiev@outlook.com";s:9:"firstName";s:6:"Viktor";s:8:"lastName";s:8:"Georgiev";}');
    }

    public function testUserCreationValidationFailed(): void
    {
        $this->expectException(ValidationFailedException::class);

        $this->userService
            ->expects(self::never())
            ->method('create');

        $controller = new UserController($this->userService);

        $request = new Request();
        $request->headers->set('Content-type', 'application/json');
        $request->setMethod('POST');
        $request->request->set('firstName', 'Viktor');
        $request->request->set('lastName', 'Georgiev');

        $controller->create($request);
    }
}
