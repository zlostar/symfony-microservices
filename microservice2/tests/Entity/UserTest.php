<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\User;
use JMS\Serializer\Exception\ValidationFailedException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @internal
 */
class UserTest extends TestCase
{
    public function testUserCreation(): void
    {
        $request = new Request();
        $request->headers->set('Content-type', 'application/json');
        $request->setMethod('POST');
        $request->request->set('email', 'viktor.georgiev@outlook.com');
        $request->request->set('firstName', 'Viktor');
        $request->request->set('lastName', 'Georgiev');

        $user = User::createUser($request);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($user->getEmail(), 'viktor.georgiev@outlook.com');
        $this->assertEquals($user->getFirstName(), 'Viktor');
        $this->assertEquals($user->getLastName(), 'Georgiev');
    }

    public function testUserCreationNoEmail(): void
    {
        $request = new Request();
        $request->headers->set('Content-type', 'application/json');
        $request->setMethod('POST');
        $request->request->set('firstName', 'Viktor');
        $request->request->set('lastName', 'Georgiev');

        $this->expectException(ValidationFailedException::class);

        User::createUser($request);
    }

    public function testUserCreationNoFirstName(): void
    {
        $request = new Request();
        $request->headers->set('Content-type', 'application/json');
        $request->setMethod('POST');
        $request->request->set('email', 'viktor.georgiev@outlook.com');
        $request->request->set('lastName', 'Georgiev');

        $user = User::createUser($request);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($user->getEmail(), 'viktor.georgiev@outlook.com');
        $this->assertEquals($user->getFirstName(), null);
        $this->assertEquals($user->getLastName(), 'Georgiev');
    }

    public function testUserCreationNoLastName(): void
    {
        $request = new Request();
        $request->headers->set('Content-type', 'application/json');
        $request->setMethod('POST');
        $request->request->set('email', 'viktor.georgiev@outlook.com');
        $request->request->set('firstName', 'Viktor');

        $user = User::createUser($request);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($user->getEmail(), 'viktor.georgiev@outlook.com');
        $this->assertEquals($user->getFirstName(), 'Viktor');
        $this->assertEquals($user->getLastName(), null);
    }
}
