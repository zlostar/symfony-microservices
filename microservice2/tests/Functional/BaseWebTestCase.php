<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseWebTestCase extends WebTestCase
{
    private array $headers = [
        'CONTENT_TYPE' => 'application/json',
        'HTTP_X_TEST' => true,
    ];


    protected function request(string $method, string $uri, array $content = [], array $headers = []): Response
    {
        $headers = array_merge($this->headers, $headers);
        $json = json_encode($content, JSON_THROW_ON_ERROR);
        $client = $this->buildClient();
        $client->request($method, $uri, [], [], $headers, $json);

        return $client->getResponse();
    }

    protected function buildClient(array $options = [], array $server = []): KernelBrowser
    {
        return static::createClient($options, $server);
    }
}
