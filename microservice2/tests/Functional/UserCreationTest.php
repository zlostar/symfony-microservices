<?php

namespace App\Tests\Functional;

class UserCreationTest extends BaseWebTestCase
{
    private const URL = '/api/users';

    public function testUserCreation(): void
    {
        $client = $this->buildClient();

        $client->request('POST', self::URL, [
            'email' => 'viktor.georgiev@outlook.com',
            'firstName' => 'Viktor',
            'lastName' => 'Georgiev',
        ]);

        self::assertResponseStatusCodeSame(200);
        $this->assertEquals($client->getResponse()->getContent(), 'O:22:"App\Message\UserCreate":3:{s:5:"email";s:27:"viktor.georgiev@outlook.com";s:9:"firstName";s:6:"Viktor";s:8:"lastName";s:8:"Georgiev";}');
    }
}