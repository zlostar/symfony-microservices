<?php

declare(strict_types=1);

namespace App\Tests\Message;

use App\Message\UserCreate;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
class UserCreateTest extends TestCase
{
    public function testUserCreateEmpty(): void
    {
        $user = new UserCreate(null, null, null);

        $this->assertInstanceOf(UserCreate::class, $user);
        $this->assertEquals($user->getEmail(), null);
        $this->assertEquals($user->getFirstName(), null);
        $this->assertEquals($user->getLastName(), null);
    }

    public function testUserCreateNotEmpty(): void
    {
        $user = new UserCreate('viktor.georgiev@outlook.com', 'Viktor', 'Georgiev');

        $this->assertInstanceOf(UserCreate::class, $user);
        $this->assertEquals($user->getEmail(), 'viktor.georgiev@outlook.com');
        $this->assertEquals($user->getFirstName(), 'Viktor');
        $this->assertEquals($user->getLastName(), 'Georgiev');
    }
}
