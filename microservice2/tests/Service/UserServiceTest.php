<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Message\UserCreate;
use App\Repository\UserRepositoryInterface;
use App\Service\UserService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @internal
 */
class UserServiceTest extends TestCase
{
    private UserRepositoryInterface $userRepository;
    private MessageBusInterface $messageBus;

    public function setUp(): void
    {
        $this->userRepository = $this->createMock(UserRepositoryInterface::class);
        $this->messageBus = $this->createMock(MessageBusInterface::class);
    }

    public function testUserService(): void
    {
        $message = new UserCreate(
            'viktor.georgiev@outlook.com',
            'Viktor',
            'Georgiev'
        );

        $this->userRepository
            ->expects(self::once())
            ->method('create');

        $this->messageBus
            ->expects(self::once())
            ->method('dispatch')
            ->willReturn(new Envelope($message, []));

        $service = new UserService($this->messageBus, $this->userRepository);

        $service->create($message);
    }
}