<?php

namespace App\Service;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\UserCreate;

interface UserServiceInterface
{
    public function create(UserCreate $userCreate): Envelope;
}