<?php

namespace App\Service;

use App\Repository\UserRepositoryInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\UserCreate;

class UserService implements UserServiceInterface
{
    public function __construct(
        private readonly MessageBusInterface $messageBus,
        private readonly UserRepositoryInterface $userRepository
    )
    {
    }

    public function create(UserCreate $userCreate): Envelope
    {
        $this->userRepository->create($userCreate);

        return $this->messageBus->dispatch(
            message: $userCreate
        );
    }
}