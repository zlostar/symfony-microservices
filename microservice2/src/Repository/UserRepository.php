<?php

namespace App\Repository;

use App\Message\UserCreate;

class UserRepository implements UserRepositoryInterface
{
    public function create(UserCreate $userCreate): void
    {
        file_put_contents($this->getPath(), serialize($userCreate) . "\n", FILE_APPEND | LOCK_EX);
    }

    public function getPath()
    {
        return __DIR__ . '/../../var/storage/users.txt';
    }
}