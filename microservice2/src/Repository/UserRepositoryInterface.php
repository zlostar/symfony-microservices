<?php

namespace App\Repository;

use App\Message\UserCreate;

interface UserRepositoryInterface
{
    public function create(UserCreate $userCreate): void;
}