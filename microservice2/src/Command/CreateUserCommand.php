<?php
declare(strict_types=1);

namespace App\Command;

use App\Message\UserCreate;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(
    name: "app:create-user",
    description: 'Creates a new user.',
    hidden: false,
    aliases: ['app:add-user']
)]
class CreateUserCommand extends Command
{
    public function __construct(private readonly MessageBusInterface $messageBus, string $name = null)
    {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $email = "viktor.georgiev@outlook.com";
        $firstName = "Viktor";
        $lastName = "Georgiev";

        $this->messageBus->dispatch(
            message: new UserCreate($email, $firstName, $lastName)
        );

        return Command::SUCCESS;
    }
}