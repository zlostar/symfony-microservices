<?php
declare(strict_types=1);

namespace App\Entity;

use JMS\Serializer\Exception\ValidationFailedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ValidatorBuilder;

class User
{
    #[Assert\NotBlank]
    private ?string $email;
    private ?string $firstName;
    private ?string $lastName;

    private function __construct()
    {

    }

    public static function createUser(Request $request): User
    {
        $validator = (new ValidatorBuilder())->enableAnnotationMapping()->getValidator();

        $user = new User();
        $user->email = $request->request->get('email') ?? null;
        $user->firstName = $request->request->get('firstName') ?? null;
        $user->lastName = $request->request->get('lastName') ?? null;

        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            throw new ValidationFailedException($errors);
        }

        return $user;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }
}