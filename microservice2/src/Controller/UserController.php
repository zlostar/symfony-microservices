<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\UserServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Message\UserCreate;

#[Route('/api', name: 'api_')]
class UserController extends AbstractController
{
    public function __construct(
        private readonly UserServiceInterface $userService
    )
    {
    }

    #[Route('/users', name: 'user_create', methods:['post'])]
    public function create(Request $request): Response
    {
        $user = User::createUser($request);
        $message = new UserCreate(
            $user->getEmail(),
            $user->getFirstName(),
            $user->getLastName()
        );

        $envelope = $this->userService->create($message);

        return new Response(serialize($envelope->getMessage()), 200);
    }
}